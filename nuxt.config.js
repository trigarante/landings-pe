
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
     htmlAttrs: { lang: 'es-PE' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
     script: [
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '../img/favicon.png'},
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: ['static/css/style.min.css','~/node_modules/bootstrap/dist/css/bootstrap.css',],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
   { src: '~/plugins/filters.js', ssr: false }]
  ,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    ['nuxt-validate', { lang: 'es' }],
    ['@nuxtjs/google-tag-manager', { id: 'GTM-K52RR7K' }]
  ],
  /*
  ** Build configuration
  */
  build: {



    extend (config, ctx) {
    }

  },
  render: {
        http2: { push: true },
        resourceHints: true,
        compressor:{threshold:9}
    }
}
