import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (marca,
                                     modelo,
                                     idModelo,
                                     descripcion,
                                     sumaAsegurada,
                                     nombre,
                                     telefono,
                                     correo,
                                     grupo,
                                     impuesto,
                                     primaNeta,
                                     primaTotal,
                                     url
) {

  return axios({
    method: "post",
    url: configDB.baseUrl+  '/save',
    data: {
      marca,
      modelo,
      idModelo,
      descripcion,
      sumaAsegurada,
      nombre,
      telefono,
      correo,
      grupo,
      impuesto,
      primaNeta,
      primaTotal,
      url
    }
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default cotizacionService
