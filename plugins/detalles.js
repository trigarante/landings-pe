import axios from "axios";
import configDB from "./configBase";

const detallesService ={}

detallesService.search=function (idModelo) {
  return axios({
    method: "get",
    url: configDB.baseUrl+  '/grupo/'+idModelo,})
    .then(
      res => res.data
    )
    .catch(
      err => console.error(err)
    );
}
export default detallesService
