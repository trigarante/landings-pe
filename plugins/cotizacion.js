import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (
  idModelo,
  sumaAsegurada,
  grupo
) {

  return axios({
    method: "post",
    url: configDB.baseUrl + '/cotizar',
    data: {
      idModelo,
      sumaAsegurada,
      grupo
    }
  })
    .then(res => res.data)
    .catch(err => console.error("Problema al cotizar con "+" "+err));
}
export default cotizacionService
