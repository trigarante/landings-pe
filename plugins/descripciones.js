import autosService from './ws-autos'
import axios from "axios";
import configDB from "./configBase";

const descripcionesService ={}

descripcionesService.search=function (idMarca) {
  return axios({
    method: "get",
    url: configDB.baseUrl+  '/modelo/'+idMarca,})
    .then(
      res => res.data
    )
    .catch(
      err => console.error(err)
    );
}
export default descripcionesService
