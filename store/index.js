import Vuex from 'vuex'
import database from '~/plugins/saveData'
import cotizacion from '~/plugins/cotizacion'

const createStore = () => {
  return new Vuex.Store({
    state: {
      config: {
        aseguradora: '',
        cotizacion: true,
        notFound: false,
        emision: true,
        descuento: 0,
        telefonoAS: "47495114",
        grupoCallback: '',
        from: '',
        idPagina: 0,
        idAseguradora: 0,
        img: "qualitas.webp",
        alert: true,
        asgNombre: "Quálitas",
        promo: "Obtén hasta",
        desc: "20% de descuento +",
        msi: "3 y 6 Meses Sin Intereses",
        dominio: "qualitasseguros.mx",
        domV2: "qualitasautos.mx",
        domV3: "qualitassegurosautos.com",
        url: ''
      },
      auto: {
        'default': {
          idPagina: 1190,
          idCampana: 70,
          numTelefono: "5547495127",
          grupoCallback: "VN AGREGADORES",
          aseguradora: "QUALITAS",
          idSubRamo: 50,
          nam: "QUALITAS",
          routes: {
            email: {
              idPagina: 1190,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            },
            facebook: {
              idPagina: 1190,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            },
            timeline: {
              idPagina: 1190,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            }
          }
        },
        'hdi': {
          idPagina: 92,
          idCampana: 70,
          numTelefono: "5547495127",
          grupoCallback: "VN AGREGADORES",
          aseguradora: "HDI",
          idSubRamo: 32,
          nam: "HDI",
          routes: {
            email: {
              idPagina: 120,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            },
            facebook: {
              idPagina: 119,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            },
            timeline: {
              idPagina: 212,
              idCampana: 70,
              numTelefono: '5547495127',
              grupoCallback: 'VN AGREGADORES'
            }
          }
        },
        'qualitas' : {
          'idPagina': 7,
          'idCampana': 1,
          'numTelefono': "5547495114",
          'grupoCallback': "VN QUALITAS",
          'aseguradora': "QUALITAS",
          'idSubRamo': 35,
          'nam': "Qualitas",
          'routes': {
            'email': {
              'idPagina': 111,
              'idCampana': 1,
              'numTelefono': '5547495114',
              'grupoCallback': 'VN QUALITAS'
            },
            'facebook': {
              'idPagina': 110,
              'idCampana': 1,
              'numTelefono': '5547495114',
              'grupoCallback': 'VN QUALITAS'
            },
            'timeline': {
              'idPagina': 210,
              'idCampana': 1,
              'numTelefono': '5547495114',
              'grupoCallback': 'VN QUALITAS'
            }
          }
        },
        'mapfre' : {
          'idPagina': 32,
          'idCampana': 106,
          'numTelefono': "5547495111",
          'grupoCallback': "VN Mapfre",
          'aseguradora': "MAPFRE",
          'idSubRamo': 43,
          'nam': "MAPFRE",
          'routes': {
            'email': {
              'idPagina': 36,
              'idCampana': 106,
              'numTelefono': '5547495111',
              'grupoCallback': 'VN Mapfre'
            },
            'facebook': {
              'idPagina': 33,
              'idCampana': 106,
              'numTelefono': '5547495111',
              'grupoCallback': 'VN Mapfre'
            },
            'timeline': {
              'idPagina': 996,
              'idCampana': 106,
              'numTelefono': '5547495111',
              'grupoCallback': 'VN Mapfre'
            }
          }
        },
      },
      cotizacion:{},
      aseguradoras: {
        'HDI': {
          cotiza: true,
          cotizacionLimitada: false,
          descuento: 15,
          buscar: true,
          emite: true,
          calificacion: {
            estrellas: 4.5,
            precio: 92,
            cobertura: 99,
            beneficio: 98
          },
          promoGracias: "6 Y 12 MSI TODAS LAS TDC MENOS SANTANDER E INBURSA"
        },
        'MAPFRE' : {
          'cotiza': false,
          'cotizacionLimitada': false,
          'descuento': 0,
          'buscar': true,
          'emite': true,
          'calificacion': {
            'estrellas': 4.5,
            'precio': 92,
            'cobertura': 99,
            'beneficio': 98
          },
          'promoGracias': "Maneja 12 MSI Banamex,American Express,Bancomer,Santander,Banorte,HSBC"
        },
        'QUALITAS' : {
          'cotiza': true,
          'cotizacionLimitada': false,
          'descuento': 20,
          'buscar': true,
          'emite': true,
          'calificacion': {
            'estrellas': 4.5,
            'precio': 92,
            'cobertura': 99,
            'beneficio': 98
          },
          'promoGracias': "3 Y 6 MSI NO PARTICIPAN LAS TARJETAS BANAMEX E INBURSA"
        },
      },
      formData: {
        aseguradora: '',
        marca: '',
        idMarca: '',
        modelo: '',
        idModelo: '',
        descripcion: '',
        idDescripcion: '',
        subDescripcion: '',
        detalle: '',
        detalleid: '',
        codigoPostal: '',
        nombre: '',
        apellidoPaterno: '',
        apellidoMaterno: '',
        telefono: '',
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        precio: '',
        sumaAsegurada: '30000'
      },
    },
    mutations: {
      cotizar: function (state) {
        console.log('Cotizando');
        cotizacion.search(
          state.formData.idModelo,
          state.formData.sumaAsegurada,
          state.formData.grupo
        ).then(resp => {
          state.cotizacion = resp;
          this.commit('saveData',state.cotizacion);
        });
      },

      saveData: function (state,cot) {
        console.log('Guardando Datos');
        var url = state.config.url;
        database.search(
          state.formData.marca,
          state.formData.modelo,
          state.formData.idModelo,
          state.formData.descripcion,
          state.formData.sumaAsegurada,
          state.formData.nombre,
          state.formData.telefono,
          state.formData.correo,
          state.formData.grupo,
          cot['impuesto'],
          cot['primaNeta'],
          cot['primaTotal'],
          url.toLowerCase()
        )
          .then(resp => {
            state.solicitud = resp;
          });
      }
    }
  })
};
export default createStore
